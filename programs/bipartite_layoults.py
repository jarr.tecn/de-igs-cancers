import pandas as pd
import upsetplot
import matplotlib.pyplot as plt
from collections import Counter
import networkx as nx
from itertools import combinations, product, chain
from collections import Counter

get_projection= nx.algorithms.bipartite.projection.overlap_weighted_projected_graph
node_redundancy= nx.algorithms.bipartite.redundancy.node_redundancy
closeness_centrality= nx.algorithms.bipartite.centrality.closeness_centrality
clustering= nx.algorithms.bipartite.cluster.clustering
get_matching= nx.algorithms.bipartite.matching.maximum_matching
edge_cover= nx.algorithms.bipartite.covering.min_edge_cover

##############################
#                            #
# Custom bipartites layoults #
#                            #
##############################

def score_sort(nodes_list, scores_dict):
    """
    Returns the list of the nodes sorted by the score specified
    in the scores dict.

    If a node has no redundancy, then it is put at the
    begin of the list.
    """
    not_metirc_nodes= list(filter(lambda x : x not in scores_dict ,nodes_list))
    yes_metirc_nodes= list(filter(lambda x : x in scores_dict ,nodes_list))
    nodes_list= not_metirc_nodes + sorted(yes_metirc_nodes,
                                               key=lambda x: scores_dict[x])
    return nodes_list

def normalice_bipartite_scores(scores_dict,reference_nodes):
    """
    The scores of the reference_nodes determine a range of values.

    The scores of nodes NOT in reference_nodes will be re-computed
    in such way that their score mathc the same range of values.
    """
    D_a= {n:scores_dict[n] for n in scores_dict if n in reference_nodes}
    D_b= {n:scores_dict[n] for n in scores_dict if n not in reference_nodes}

    m_a= min(D_a.values())
    M_a= max(D_a.values())
    m_b= min(D_b.values())
    M_b= max(D_b.values())

    f= lambda x: m_a + (x-m_b)*(M_a-m_a)/(M_b-m_b)

    new_scores= D_a
    new_scores.update({n:f(i) for n,i in D_b.items()})

    return new_scores

def bipartite_layoult(B,up_nodes,down_nodes=None,up= 1,down= 0):
    """
    Returns the dictionary of positions.

    Inputs:
    B: Bipartite graph
    up_nodes: One partition of B
    down_nodes: The other partition of B

    """

    if(down_nodes==None):
        down_nodes= set(B.nodes).difference(up_nodes)

    n_up= len(up_nodes)-1
    n_down= len(down_nodes)-1

#    pos= {node:(i/n_up, up) for i,node in enumerate(up_nodes)}
    alpha= 0.25
    #m= (1+2*alpha)/n_up
    #fff= lambda x: m*x
    pos= {node:(i*(1+2*alpha)/n_up - alpha, up) for i,node in enumerate(up_nodes)}
    pos.update({node:(i/n_down, down) for i,node in enumerate(down_nodes)})

    return pos

def plot_bipartite(B,
                   up_nodes,
                   down_nodes=None,
                   ax= None,
                   scores_dict= None,
                   color_by_score= False,
                   n_colors= None,
                   e_colors= None,
                   e_cmap= plt.cm.Reds,
                   n_cmap= plt.cm.Reds,
                   font_size= None,
                  ):

    if(down_nodes==None):
        down_nodes= set(B.nodes).difference(up_nodes)
    if(ax==None):
        _, ax = plt.subplots()
    if(scores_dict==None):
        not_metirc_nodes= []
    else:
        up_nodes= score_sort(up_nodes, scores_dict)
        down_nodes= score_sort(down_nodes, scores_dict)
        not_metirc_nodes= list(filter(lambda x : x not in scores_dict ,B.nodes))
    if(color_by_score):
        n_colors= [scores_dict.get(n,0) for n in B.nodes]
    #n_colors= [B.degree[n] for n in B] #[scores_dict[n] for n in B.nodes]

    pos= bipartite_layoult(B,up_nodes,down_nodes)
    options = {
        "node_color": n_colors,
        "cmap": n_cmap,
        "edge_color": e_colors,
        "edge_cmap": e_cmap,
        "font_size": 10000,
    }


    nx.draw(B,pos,ax,**options)

    for node in not_metirc_nodes:
        x,y= pos[node]
        plt.plot(x,y,"o",color="purple")

    for selected_gene in up_nodes:
        x,y= pos[selected_gene]
        plt.text(x,y,selected_gene,
                 {'ha': 'left', 'va': 'bottom'},
                 rotation=90,
                 size= 25,
                )

    for selected_gene in down_nodes:
        x,y= pos[selected_gene]
        plt.text(x,y,selected_gene,
                 {'ha': 'left', 'va': 'top'},
                 rotation= -90,
                 size= 100,
                )
    