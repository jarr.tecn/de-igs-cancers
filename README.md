<img src="figures/Complete-Color-background_White-Horizontal.png" align="left" width="300"/>

# Deciphering the tissue-specific regulatory role of intronless genes across cancers

**Katia Aviña-Padilla<sup>1</sup>, José Antonio Ramírez-Rafael<sup>1</sup>, Gabriel Emilio Herrera-Oropeza<sup>2</sup>, Guillermo Romero<sup>3</sup>, Octavio Zambada-Moreno<sup>1</sup>, Ishaan Gupta<sup>4</sup>, and Maribel Hernández-Rosales<sup>1</sup>**

<sup>1</sup>CINVESTAV-Irapuato; <sup>2</sup>King's College London; <sup>3</sup>Data-Pop Alliance; <sup>4</sup>Indian Institute of Technology, Delhi. **| [maribel.hr@cinvestav.m](mailto:maribel.hr@cinvestav.mx)**

***

This repository contains code created for the analysis of differentially expressed intronless genes in different tumor tissues. Results obtained here were wrapped, analyzed, and interpreted in the paper titled like this project. Preprint will be soon available in [bioRxiv](https://www.biorxiv.org/content/10.1101/2022.02.21.481319v1).

- [Cancer-specific differentialy expresed intronless genes](programs/deseases_multiplex.ipynb)
- [Fidelity of edges retrieved from string-DB](programs/confidencialidad.ipynb)
- [PPI network metrics](programs/ppi_network_statistics.ipynb)
- [PPI network visualization](programs/ppi_multilayer.ipynb)
- [Differentially expresed genes](programs/DE_analysis_TCGA.R)
- [Programs](programs/)

